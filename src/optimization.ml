(* @requires: Reçoit une file 'q_clear' non vide initialement, une file 'q_fill' et
   @ensures: On ne renvoie rien mais on modifie la file 'q_fill' avec les éléments
   de de 'q_clear' qui sera vidée. 'q_fill' contiendra le 'node_to_replace' avant 
   la ville nommée 'new_node', mais avec le même ordre des éléments de 'q_fill'
 *)
let rec fill_and_clear_queue q_clear q_fill node_to_replace new_node index inc_index = 
    if Queue.is_empty q_clear then
        ()
    else
        let a = Queue.pop q_clear in
        (* let _ = Printf.printf "fill_and_clear_queue : %d -> %d (index : %d)\n" new_node a index in *)
        if a = new_node && inc_index = index then
            (* let _ = Printf.printf "fill\n" in *)
            let _ = Queue.push node_to_replace q_fill in
            let _ = Queue.push a q_fill in
            fill_and_clear_queue q_clear q_fill node_to_replace new_node index (inc_index + 1)
        else
            let _ = Queue.push a q_fill in
            fill_and_clear_queue q_clear q_fill node_to_replace new_node index (inc_index + 1)


(* @requires: Pas de conditions particulières
   @ensures: Renvoie un couple (distance_mise_à_jour, ville) si le noeud que l'on veut remplacer
   dans l'optimisation de repositionnement est meilleur que la distance précédente.
   Si on prend un trajet A-B-C, on enlève qu'on essaie de replacer quelque part d'autres de sorte
   à réduire la distance précédente
 *)
 let rec replace_node_to_path adj_function adj node_to_replace previous_node new_node_index new_node path_out path_tmp update_distance current_distance current_index = 
    try
        let (getter, is_neighbor_of) = adj_function in
        let node_to_analyze = Queue.pop path_out in

        let bool_neighbor = is_neighbor_of adj previous_node node_to_replace && is_neighbor_of adj node_to_replace node_to_analyze in
        let new_distance = update_distance -. (getter adj previous_node node_to_analyze) +. (getter adj previous_node node_to_replace) +. (getter adj node_to_replace node_to_analyze) in
        let _ = Queue.push node_to_analyze path_tmp in

        (* let _ = Printf.printf "replace_node_to_path: Noeud a inserer : %d -> (prev : %d) %d\n" node_to_replace previous_node new_node in *)
        if bool_neighbor && new_distance < current_distance then
            replace_node_to_path adj_function adj node_to_replace node_to_analyze current_index node_to_analyze path_out path_tmp update_distance new_distance (current_index + 1)
        else
            replace_node_to_path adj_function adj node_to_replace node_to_analyze new_node_index new_node path_out path_tmp update_distance current_distance (current_index + 1)
    with Queue.Empty -> 
        let _ = fill_and_clear_queue path_tmp path_out node_to_replace new_node new_node_index 0 in
        (current_distance, new_node_index)



(* @requires: Pas de conditions particulières
   @ensures: Renvoie un chemin suivant l'optimisation de repositionnement
 *)
let repos_optimize adj_function adj path current_distance = 
    let path_out = Queue.create () in
    let a = Queue.pop path in
    let b = Queue.pop path in

    let rec repos_optimize_rec adj_function adj path path_out a b current_distance = 
        if Queue.is_empty path then
            let _ = Queue.push a path_out in
            let _ = Queue.push b path_out in
            (path_out, current_distance)
        else
            let (getter, is_neighbor_of) = adj_function in
            let c = Queue.pop path in
            if is_neighbor_of adj a c then
                let (update_distance, new_node_index) = replace_node_to_path adj_function adj b c (-1) (-1) path (Queue.create ()) (current_distance -. (getter adj a b) -. (getter adj b c) +. (getter adj a c)) current_distance 0 in
                if new_node_index = (-1) then
                    let _ = Queue.push a path_out in
                    repos_optimize_rec adj_function adj path path_out b c current_distance
                else
                    let _ = Queue.push a path_out in
                    let _ = Queue.push c path_out in
                    (* try *)
                        let a_new = Queue.pop path in
                        let b_new = Queue.pop path in
                        repos_optimize_rec adj_function adj path path_out a_new b_new update_distance
                    (* with Queue.Empty -> (path_out, current_distance) *)
            else
                let _ = Queue.push a path_out in
                repos_optimize_rec adj_function adj path path_out b c current_distance

    in repos_optimize_rec adj_function adj path path_out a b current_distance


(* @requires: Pas de conditions particulières
   @ensures: Renvoie un couple spécifiant si les noeuds b et c du point = (a, b, x, c, d)
   peuvent être inversés
 *)
let inverse_node adj_function adj current_distance point = 
    let (a, b, x, c, d) = point in
    let (getter, is_neighbor_of) = adj_function in
    let update_distance = current_distance -. (getter adj a b) -. (getter adj c d) +. (getter adj b d) +. (getter adj a c) in
    if update_distance < current_distance && is_neighbor_of adj a c && is_neighbor_of adj b d then
        (true, update_distance)
    else
        (false, current_distance)


(* @requires: Pas de conditions particulières
   @ensures: Renvoie un chemin suivant l'optimisation d'inversion
 *)
let inversion_optimize adj_function adj path current_distance = 
    let rec inversion_optimize_rec adj_function adj path path_out current_distance point = 
        let (result, distance) = inverse_node adj_function adj current_distance point in
        let (a, b, x, c, d) = point in
        if Queue.is_empty path then
            if result then
                let _ = Queue.push a path_out in
                let _ = Queue.push c path_out in
                let _ = Queue.push x path_out in
                let _ = Queue.push b path_out in
                let _ = Queue.push d path_out in
                (path_out, distance)
            else
                let _ = Queue.push a path_out in
                let _ = Queue.push b path_out in
                let _ = Queue.push x path_out in
                let _ = Queue.push c path_out in
                let _ = Queue.push d path_out in
                (path_out, current_distance)
        else
            let _ = Queue.push a path_out in
            let new_node = Queue.pop path in
            if result then
                inversion_optimize_rec adj_function adj path path_out distance (c, x, b, d, new_node)
            else
                inversion_optimize_rec adj_function adj path path_out current_distance (b, x, c, d, new_node)
                
    in 

    try
        let path_out = Queue.create () in
        let a = Queue.pop path in
        let b = Queue.pop path in
        let x = Queue.pop path in
        let c = Queue.pop path in
        let d = Queue.pop path in
        inversion_optimize_rec adj_function adj path path_out current_distance (a, b, x, c, d)
    with Queue.Empty -> (path, current_distance) 


(*****************************************************)
(*******************SELECTION DU MODE*****************)
(*****************************************************)
exception Not_Found of string

let opti_mode name_mode adj_function adj path current_distance = 
    if name_mode = "REPOSITIONNEMENT" then
        repos_optimize adj_function adj path current_distance
    else if name_mode = "INVERSION" then
        inversion_optimize adj_function adj path current_distance
    else
        raise (Not_Found name_mode) 