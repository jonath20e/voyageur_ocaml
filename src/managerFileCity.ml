open AdjacencyMatrixType;;

(* ------------------------------------------------------ *)
module TurnCity = AdjacencyMatrix.Make(struct
        type elt = float
        
        let is_neighbor_of e = 
            e >= 0.
    end);;

module MapCity = Map.Make(
    struct 
        type t = string
        let compare = compare 

    end
);;
(* ------------------------------------------------------ *)

(* @requires: pas de conditions particulières
   @ensures: retourne une string list
   Transforme une chaîne de caractère en list de chaîne de caractères 
   par un délimiteur.
   Exemple: explode " " "Salut à tous" devient ["salut";"à";"tous"].
*)
let explode delimiter str = 
    let rec explode_in delimiter str current str_len = 
        if current < str_len then 
            try 
                let n = (String.index_from str current delimiter) in
                    (String.sub str current (n - current))::(explode_in delimiter str (n + 1) str_len)
            with Not_found -> [String.sub str current (str_len - current)]
        else
            []
    in explode_in delimiter str 0 (String.length str);; 



(* @requires: Reçoit en paramètres 'str' de la forme '<départ> : <arrivée_1> ... <arrivée_k>', successor de
   type 'Incorporation.MapCityKeyInt' avec une clé entière et des valeurs associées de type SetCity 
   @ensures: Renvoie successor mise à jour avec les successeurs de <depart> dans un ensemble 'SetCity'
 *)
let explode_for_successor str successors city_index =     
    let rec explode_for_successor_rec delimiter city_index city_successor first_city str current str_len = 
        if current < str_len then 
            try 
                let n = (String.index_from str current delimiter) in
                let word = (String.sub str current (n - current)) in
                    (* ::(explode_for_successor_rec delimiter str (n + 1) str_len) *)
                if first_city = "" then
                    explode_for_successor_rec delimiter city_index city_successor word str (n + 1) str_len
                else if word = ":" then
                    explode_for_successor_rec delimiter city_index city_successor first_city str (n + 1) str_len
                else 
                    (* let _ = printf "TEST : %s %d (%s):%d\n" word current str str_len in *)
                    let a = (MapCity.find word city_index) in
                    let city_successor = Incorporation.SetCity.add a city_successor in
                    explode_for_successor_rec delimiter city_index city_successor first_city str (n + 1) str_len
            with Not_found -> 
            let word = String.sub str current (str_len - current) in
            let a = (MapCity.find word city_index) in
            let city_successor = Incorporation.SetCity.add a city_successor in        
            (first_city, city_successor)
        else
            (first_city, city_successor)
    in 
    let (city, city_index_final) = explode_for_successor_rec ' ' city_index Incorporation.SetCity.empty "" str 0 (String.length str) in 
    (* let _ = SetCity.iter (function key -> printf "%d\n" key) city_index_final in *)
    Incorporation.MapCityKeyInt.add (MapCity.find city city_index) city_index_final successors



(* @requires: instance d'un fichier ouvert, la liste des noeuds et le nombre de noeuds (villes)
   @ensures: Renvoie une liste de successeurs de type 'Map key = int -> int Set' en fonction des noeuds si le fichier
   contient : <départ> : <arrivée_1> ... <arrivée_k> comme décrit pour la phase 2
*)
let extract_successor_from_city_array instance_file city_index_map n = 
    let successors = Incorporation.MapCityKeyInt.empty in
    let rec extract_successor_from_city_array_rec instance_file successors city_index_map n current_index = 
        if current_index < n then
            try
                let successors = explode_for_successor (input_line instance_file) successors city_index_map in
                extract_successor_from_city_array_rec instance_file successors city_index_map n (current_index + 1)
            with End_of_file -> close_in instance_file ; successors
        else
            successors
    in extract_successor_from_city_array_rec instance_file successors city_index_map n 0


(* @requires: nom de fichier non vide
   @ensures: retourne un array qui contient un ensemble d'array avec 
   le nom de la ville et les coordonnées associées de la forme: 
   [| 
        [|"String1"; "flaot"; "float"|];
        [|"String2"; "flaot"; "float"|];
   |]
   Extraire la liste des villes à partir d'un fichier de la forme : 
   n
   <nom de la ville> <longitude> <lattitude> [sur n lignes] 
 *)
let extract_city_array nom_fichier =
    let f = open_in nom_fichier in
        let n = int_of_string (input_line f) in
        let city_index = MapCity.empty in
            let rec extract_city_rec n current city_index city_array =
                    if current < n then
                        try
                            let city = (input_line f) in
                            let city_list = (explode ' ' city) in
                            let city_array_from_list = (Array.of_list city_list) in
                            let city_index = MapCity.add city_array_from_list.(0) current city_index in
                            let city_array = city_array_from_list::city_array in
                            extract_city_rec n (current + 1) city_index city_array
                        with End_of_file -> close_in f ; (city_array, city_index)
                    else
                        (city_array, city_index)
    in 
    let (city_array, city_index) =  (extract_city_rec n 0 city_index []) in
    let successors = extract_successor_from_city_array f city_index n in

    (Array.of_list (List.rev city_array), successors)


(* @requires: nom d'un fichier de type 
    ONE | HULL
    RANDOM | NEAREST | FARTHEST
    REPOSITIONNEMENT | INVERSION
   @ensures: Renvoie un Array contenant pour chaque case un type de paramétrage
 *)
let get_parameters nom_fichier = 
    let f = open_in nom_fichier in
    let rec get_parameters_rec f = 
        try
            let a = (input_line f) in
            (* let _ = Printf.printf "%s " a in *)
            a::(get_parameters_rec f)
        with End_of_file -> close_in f ; []
    in Array.of_list (get_parameters_rec f)


(* @requires: Matrice d'adjacence non vide
   @ensures: Ne renvoie rien mais affiche, sous la forme d'un tableau, la matrice
 *)
let print_adj adj = TurnCity.iteri (function x -> function y -> function value ->
        if y = 0 then
            let _ = Printf.printf "\n" in 
            Printf.printf "%f (%d -> %d) \t" value x y
        else
            Printf.printf "%f (%d -> %d) \t" value x y
    ) adj;;
