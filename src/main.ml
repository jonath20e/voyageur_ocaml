open Printf;;
open ManagerFileCity;;

(* Début du programme *)
let _ = 
    (* Récupérer l'argument qui contient le chemin relatif du fichier de paramétrage.
    Le fichier doit contenir la liste de villes avec leurs 
    coordonnées GPS.
    *)
    let parameters_from_file = Sys.argv.(1) in 


    (* Récupérer l'argument qui contient le chemin relatif du fichier de villes.
    Le fichier doit contenir la liste de villes avec leurs 
    coordonnées GPS.
    *)
    let city_from_file = Sys.argv.(2) in 

    (* Génération des modes de paraùètrage sous forme d'un Array *)
    let parameters = get_parameters parameters_from_file in

    (* Récupération des villes et de leurs coordonnées "sphériques" *)
    let (city_array, successors) = extract_city_array city_from_file in

    
        (* let _ = Incorporation.MapCityKeyInt.iter (function key -> function t -> 
        printf "\n%d -> " key ; 
        Incorporation.SetCity.iter (function a -> printf "%d " a) t
        ) successors in *)

    (* Récupération des coordonnées cartésiennes *)
    let city_coordinate = Geometry.extract_coordinate city_array in 

    (* let _  = Array.iter (function a -> let (x, y, z) = a in printf "x:%f y:%f z:%f\n" x y z) city_coordinate in *)

    (* On crée une matrice non orientée à partir d'un array qui contient 
       la taille de la matrice à créer et des coordonnées de chaque ville.
       On lui renseigne ensuite une fonction qui fait le calcul de distance entre deux 
       noeuds des villes qu'il reçoit en paramètres.
     *)
    let adj = TurnCity.make_undirected_matrix (function x -> function y -> function node_array ->
        if (Incorporation.is_phase2 successors) then
            try
                if (Incorporation.SetCity.mem y (Incorporation.MapCityKeyInt.find x successors)) then
                    Geometry.euclidean_distance3d node_array.(x) node_array.(y)
                else
                    -1.
            with Not_found -> -1.
        else if x = y then
            -1.
        else
            Geometry.euclidean_distance3d node_array.(x) node_array.(y)
    ) city_coordinate (-1.) in

    (* v = (state, path, number_true_state) *)
    (* let v = InitPath.init_one (TurnCity.length adj) in *)
    let (state, q, number_one_state, first_city, current_distance, current_city) = InitPath.init_path_mode parameters.(0) (TurnCity.get) adj city_coordinate (Incorporation.is_phase2 successors) in
    let v = (state, q, number_one_state) in
    
    (* Pour utiliser le système aléatoire, il faut initialiser une fois la fonction ci-dessous *)
    let _ = Random.self_init () in

    (* Résultat de l'incorporation sous forme de 'Queue' avec la distance du parcours associée *)
    let (path, distance) = Incorporation.incorp_mode parameters.(1) (TurnCity.get, TurnCity.is_neighbor_of) adj (TurnCity.length adj) v (Array.make (TurnCity.length adj) 0.) successors current_distance current_city first_city in

    let path_copy = Queue.copy path in

    let (path_out, distance_opt) = Optimization.opti_mode parameters.(2) (TurnCity.get, TurnCity.is_neighbor_of) adj path_copy distance in
    
    (* Pour voir l'état du chemin sans optimisation *)
    (* printf "%f km :" (distance /. 1000.) ;
    Queue.iter (function x -> printf " %s" city_array.(x).(0)) path ;
    printf "\n" ; *)

    (* Pour tester si la distance est respectée *)
    (* let path_test = Queue.copy path_out in
    let index = Queue.pop path_test in 
    let (_, distance) = Queue.fold (function accu -> function x -> 
        let (node, distance) = accu in
        let distance = distance +. (TurnCity.get adj node x) in 
        (x, distance)
    ) (index, (0.)) path_test in *)
    (* printf "%f \n" distance ; *)
    printf "%f km :" (distance_opt /. 1000.) ;
    Queue.iter (function x -> printf " %s" city_array.(x).(0)) path_out ;
    printf "\n" ;

    (* Permet d'afficher la matrice d'adjacence *)
    (* ManagerFileCity.print_adj adj *)

    
;;



