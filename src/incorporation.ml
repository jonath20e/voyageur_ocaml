(*****************************************************)
(*******************Fonctions phase 1*****************)
(*****************************************************)


(* @requires: Reçoit une liste d'états de l'insertion
   @ensures: Renvoie une index de ville non encore présente dans le chemin de 
   manière aléatoire
   Attention: peut amener à une boucle infinie si tous les noeuds ont été visités
   exactement une fois
*)
let rec get_index_random state = 
    let n = Array.length state in
    let index = Random.int n in
    if state.(index) = 1 then
        get_index_random state
    else
        index
    

(* @requires: Requiert 'adj_function' de la forme (getter, is_neighbor_of), adj une matrice d'adjacence et un 'triplet' 
   de la forme (state, path, number_true_state)
   @ensures: Renvoie un chemin et sa distance
   Crée un chemin aléatoire par rapport à la matrice d'adjacence 
 *)
let rec create_path_random adj_function adj triplet current_distance current_city first_city = 
    let (get, is_neighbor_of) = adj_function in
    let (state, path, number_true_state) = triplet in
    let n = Array.length state in

    if n = number_true_state then
        let _ = Queue.push first_city path in
        let current_distance = current_distance +. (get adj current_city first_city)  in
        (path, current_distance)
    else
        let index1 = get_index_random state in
        let index2 = get_index_random state in
        
        if is_neighbor_of adj current_city index1 && current_distance +. (get adj current_city index1) < current_distance +. (get adj current_city index2) then
                let _ = Array.set state index1 1 in
                let _ = Queue.push index1 path in
                let update_distance = current_distance +. (get adj current_city index1) in

                create_path_random adj_function adj (state, path, number_true_state + 1) update_distance index1 first_city
       
        else if is_neighbor_of adj current_city index2 && current_distance +. (get adj current_city index1) >= current_distance +. (get adj current_city index2) then
                let _ = Array.set state index2 1 in
                let _ = Queue.push index2 path in
                let update_distance = current_distance +. (get adj current_city index2) in

                create_path_random adj_function adj (state, path, number_true_state + 1) update_distance index2 first_city
       
        else
            create_path_random adj_function adj triplet current_distance current_city first_city


(* @requires: 'operator' est une fonction qui reçoit deux valeurs à comparer et renvoie un booléen en retour.
   Si operator est >, on recevra un index qui correspondra une ville manquante du chemin dont le chemin est le plus
   court par rapport à la ville courante "index".
   @ensures: Renvoie un entier correspondant à un index existant de la matricde d'adjcacene 'adj'
 *)
let operator_index_pos operator adj getter state index first_city = 
    let rec operator_index_pos_rec operator adj getter state index inc_index current_index = 
        let n = Array.length state in
        if n = inc_index then
            current_index
        else if state.(inc_index) >= 1 then
            operator_index_pos_rec operator adj getter state index (inc_index + 1) current_index
        else
            if current_index = first_city then
                operator_index_pos_rec operator adj getter state index (inc_index + 1) inc_index
            else
                if operator (getter adj index current_index) (getter adj index inc_index) then
                    operator_index_pos_rec operator adj getter state index (inc_index + 1) inc_index
                else    
                    operator_index_pos_rec operator adj getter state index (inc_index + 1) current_index
    in operator_index_pos_rec operator adj getter state index 0 first_city


(* @requires: Requiert 'adj_function' de la forme (getter, is_neighbor_of), adj une matrice d'adjacence et un 'triplet' 
   de la forme (state, path, number_true_state)
   @ensures: Renvoie un chemin et sa distance
   Crée un le chemin avec une insertion de villes la plus courte possible par rapport à la matrice d'adjacence 
 *)
let rec create_path_nearest adj_function adj triplet current_distance current_city first_city = 
    let (get, is_neighbor_of) = adj_function in
    let (state, path, number_true_state) = triplet in
    let n = Array.length state in

    if n = number_true_state then
        let _ = Queue.push first_city path in
        let current_distance = current_distance +. (get adj current_city first_city)  in
        (path, current_distance)
    else
        let index = operator_index_pos (>) adj get state current_city first_city in
        let _ = Array.set state index 1 in
        let _ = Queue.push index path in
        let update_distance = current_distance +. (get adj current_city index) in

        create_path_nearest adj_function adj (state, path, number_true_state + 1) update_distance index first_city


(* @requires: Requiert 'adj_function' de la forme (getter, is_neighbor_of), adj une matrice d'adjacence et un 'triplet' 
   de la forme (state, path, number_true_state)
   @ensures: Renvoie un chemin et sa distance
   Crée un le chemin avec une insertion de villes la plus longue possible par rapport à la matrice d'adjacence 
 *)
 let rec create_path_farthest adj_function adj triplet current_distance current_city first_city = 
    let (get, is_neighbor_of) = adj_function in
    let (state, path, number_true_state) = triplet in
    let n = Array.length state in

    if n = number_true_state then
        let _ = Queue.push first_city path in
        let current_distance = current_distance +. (get adj current_city first_city)  in
        (path, current_distance)
    else
        let index = operator_index_pos (<) adj get state current_city first_city in
        let _ = Array.set state index 1 in
        let _ = Queue.push index path in
        let update_distance = current_distance +. (get adj current_city index) in

        create_path_farthest adj_function adj (state, path, number_true_state + 1) update_distance index first_city

(*****************************************************)
(*******************Fonctions phase 2*****************)
(*****************************************************)
module MapCityKeyInt = Map.Make(
    struct
        type t = int
        let compare = compare
    end
);;
module SetCity = Set.Make(
        struct 
        type t = int
        let compare = compare 

    end
);;
module SetCityDistance = Set.Make(
        struct 
        type t = int * float
        let compare a b = 
            let (_, y1) = a in
            let (_, y2) = b in
            compare y1 y2 

    end
);;


(* @requires: Reçoit en paramètres un Array 'state' qui décrit combien de fois un noeud a été parcouru
   et un noeud 'node'
   @ensures: Si le noeud a été parcrouru une fois ou plus, on dit qu'il a été marqué donc on renvoie 'true'
   Vérifie si une ville a déjà été visitée ou "marquée"
 *)
let is_mark state node = state.(node) > 0


(* @requires: 'successor' = ensemble des successeurs de 'current_node'
   @ensures: Renvoie un ensemble de villes qui sont voisins de 'current_node' dans 'successor' et non encore visitées
 *)
let get_next_nodes_not_marked adj_function adj state successor current_node = 
    let (getter, is_neighbor_of) = adj_function in
    let successor_with_distance_not_marked = SetCityDistance.empty in
    let (_, successor_with_distance_not_marked) = SetCity.fold (function node -> function a -> 
        let (current_node, successor_with_distance_not_marked) = a in
        if is_neighbor_of adj current_node node && is_mark state node = false then
            let successor_with_distance_not_marked = SetCityDistance.add (node, (getter adj current_node node)) successor_with_distance_not_marked in
            (current_node, successor_with_distance_not_marked)
        else
            (current_node, successor_with_distance_not_marked)
    ) successor (current_node, successor_with_distance_not_marked) in successor_with_distance_not_marked


(* @requires: 'successor' = ensemble des successeurs de 'current_node'
   @ensures: Vérifie si des villes qui sont voisins de 'current_node' et non encore visitées
 *)
 let check_nodes_not_marked adj_function adj state successor current_node = 
    let (getter, is_neighbor_of) = adj_function in
    let (_, check) = SetCity.fold (function node -> function a -> 
        let (current_node, check) = a in
        if is_neighbor_of adj current_node node && is_mark state node = false then
            (current_node, check || true)
        else
            (current_node, check || false)
    ) successor (current_node, false) in check


(* @requires: 'successor' = ensemble des successeurs de 'current_node'
   @ensures: Renvoie un ensemble de villes qui sont voisins de 'current_node' dans 
   'successor' et visitées une seule fois
 *)
 let search_node_with_one_state adj_function adj successor state current_node = 
    let (getter, is_neighbor_of) = adj_function in
    let successor_with_distance_not_marked = SetCityDistance.empty in
    let (_, successor_with_distance_not_marked) = SetCity.fold (function node -> function a -> 
        let (current_node, successor_with_distance_not_marked) = a in
        if is_neighbor_of adj current_node node && state.(node) = 1 then
            let successor_with_distance_not_marked = SetCityDistance.add (node, (getter adj current_node node)) successor_with_distance_not_marked in
            (current_node, successor_with_distance_not_marked)
        else
            (current_node, successor_with_distance_not_marked)
    ) successor (current_node, successor_with_distance_not_marked) in successor_with_distance_not_marked


(* @requires: 'cumulative_distance' un 'Array',  'successor' = ensemble des successeurs de 'current_node'
   @ensures: Renvoie un ensemble à un élément, voisin de 'current_node' dont la distance cumulée dans
   'cumulative_distance' est minimale par rapport aux autres voisins de 'current_node'
 *)
let get_min_cumulative_distance cumulative_distance successor current_node getter adj = 
    let new_node = (0, -1., -1.) in
    let (_, final_node) = SetCity.fold (function node -> function a -> 
        let (current_node, node_and_distance) = a in
        let (node_to_analyze, distance, cumulative_distance_node) = node_and_distance in
        
        (* let _ = Printf.printf "cumul_new_node : %f / cumu_actu : %f (distance %f) (current_node %d -> node %d  : %f)\n" cumulative_distance.(node) cumulative_distance_node distance current_node node (getter adj current_node node) in *)
        if cumulative_distance.(node) < cumulative_distance_node || distance < 0. then
            (current_node, (node, (getter adj current_node node), cumulative_distance.(node)))
        else
            (current_node, node_and_distance)
    ) successor (current_node, new_node) in
    (* let _ = Printf.printf "\n" in
    let _ = Printf.printf "\n" in *)
    let (node, distance, cumulative_distance_node) = final_node in
    (* let _ = Printf.printf "Choix : %f\n" cumulative_distance_node in *)
    let new_one_result = SetCityDistance.empty in
    SetCityDistance.add (node, distance) new_one_result
    

(* @requires: 'successor_map' est un ensemble d'ensembles de la forme : 
   noeud n1 -> (ensemble des voisins du noeud n1)
   noeud n2 -> (ensemble des voisins du noeud n2)
   @ensures: Renvoie un ensemble de couple (noeud, distance noeud <-> current_node)
 *)
let get_next_node adj_function adj number_of_state state cumulative_distance successor_map successor current_node = 
    (* Toutes les villes ont été visitées *)
    if number_of_state = (Array.length state) then
        let result = search_node_with_one_state adj_function adj successor state current_node in
        if SetCityDistance.is_empty result then
            let (getter, _) = adj_function in
            (* let _ = SetCity.iter (function x -> Printf.printf "%d " x) successor in
            let _ = Printf.printf "\n" in *)
            get_min_cumulative_distance cumulative_distance successor current_node getter adj
        else
            result
    else
        let successor_with_distance_not_marked = get_next_nodes_not_marked adj_function adj state successor current_node in
        let rec get_next_node_rec adj_function adj successor_with_distance_not_marked successor_map state list_of_successor current_node = 
            match list_of_successor with
                |[] -> successor_with_distance_not_marked
                |x::t -> let m = MapCityKeyInt.find x successor_map in
                         let (getter, is_neighbor_of) = adj_function in
                        if check_nodes_not_marked adj_function adj state m x then
                            let successor_with_distance_not_marked = SetCityDistance.add (x, (getter adj current_node x)) successor_with_distance_not_marked in
                            (* get_next_node_rec adj_function adj successor_with_distance_not_marked state t current_node *)
                            get_next_node_rec adj_function adj successor_with_distance_not_marked successor_map state t current_node
                        else
                            get_next_node_rec adj_function adj successor_with_distance_not_marked successor_map state t current_node
        in 
        if SetCityDistance.is_empty successor_with_distance_not_marked then
            let successor_list = (SetCity.elements (MapCityKeyInt.find current_node successor_map)) in
            let result = get_next_node_rec adj_function adj (SetCityDistance.empty) successor_map state successor_list current_node in
            if SetCityDistance.is_empty result then
                let (getter, _) = adj_function in
                (* let _ = SetCity.iter (function x -> Printf.printf "%d " x) successor in
                let _ = Printf.printf "\n" in *)
                get_min_cumulative_distance cumulative_distance successor current_node getter adj
            else
                result
        else
            successor_with_distance_not_marked


(* @requires: 'successor_map' est un ensemble d'ensembles de la forme : 
   noeud n1 -> (ensemble des voisins du noeud n1)
   noeud n2 -> (ensemble des voisins du noeud n2)
   'adj_function' => (gettter, is_neighbor_of)
   @ensures: Renvoie un couple (distance_parcouru, chemin_de_toutes_les_villes) en fonction de 
   la fonction 'operand'
 *)
let rec create_path_operand adj_function adj number_of_city triplet cumulative_distance successor_map current_distance current_city first_city operand = 
    let (get, is_neighbor_of) = adj_function in
    let (state, path, number_of_state) = triplet in
    
    (* Visualisation de la pile de chemin *)
    (* let _ = Queue.iter (function x -> Printf.printf " %d " x) path in
    let _ = Printf.printf "\n" in *)

    (* Visualisation de l'état des villes *)
    (* let _ = Array.iter (function x -> Printf.printf " %d " x) state in
    let _ = Printf.printf "\n" in *)

    (* Visualisation distance cumulée *)
    (* let _ = Array.iter (function x -> Printf.printf "%f " x) cumulative_distance in
    let _ = Printf.printf "\n" in *)
    
    if number_of_city = number_of_state then
        if is_neighbor_of adj current_city first_city then
            let current_distance = current_distance +. (get adj current_city first_city)  in
            let _ = Queue.push first_city path in
            (path, current_distance)
        else
            let successor = get_next_node adj_function adj number_of_state state cumulative_distance successor_map (MapCityKeyInt.find current_city successor_map) current_city in
            let (new_node, new_distance) = operand successor in
            let current_distance = current_distance +. new_distance in
            let _ = Queue.push new_node path in
            let _ = Array.set state new_node (state.(new_node) + 1) in
            create_path_operand adj_function adj number_of_city (state, path, number_of_state) cumulative_distance successor_map current_distance new_node first_city operand
    else
        let successor = get_next_node adj_function adj number_of_state state cumulative_distance successor_map (MapCityKeyInt.find current_city successor_map) current_city in
        let (new_node, new_distance) = operand successor in
        let current_distance = current_distance +. new_distance in
        let _ = Queue.push new_node path in
        if is_mark state new_node = false then
            let _ = Array.set state new_node (state.(new_node) + 1) in
            let _ = Array.set cumulative_distance new_node current_distance in
            create_path_operand adj_function adj number_of_city (state, path, number_of_state + 1) cumulative_distance successor_map current_distance new_node first_city operand
        else
            let _ = Array.set state new_node (state.(new_node) + 1) in
            create_path_operand adj_function adj number_of_city (state, path, number_of_state) cumulative_distance successor_map current_distance new_node first_city operand


(* @requires: 'successor_map' est un ensemble d'ensembles de la forme : 
   noeud n1 -> (ensemble des voisins du noeud n1)
   noeud n2 -> (ensemble des voisins du noeud n2)
   'adj_function' => (gettter, is_neighbor_of)
   @ensures: Renvoie un couple (distance_parcouru, chemin_de_toutes_les_villes).
   Crée un le chemin avec une insertion de villes aléatoire par rapport à la matrice d'adjacence 
 *)
let rec create_path_random_phase2 adj_function adj number_of_city triplet cumulative_distance successor_map current_distance current_city first_city = 
    create_path_operand adj_function adj number_of_city triplet cumulative_distance successor_map current_distance current_city first_city (function t -> 
        let a = Array.of_list (SetCityDistance.elements t) in
        let index = Random.int (Array.length a) in
        a.(index)
    )


(* @requires: 'successor_map' est un ensemble d'ensembles de la forme : 
   noeud n1 -> (ensemble des voisins du noeud n1)
   noeud n2 -> (ensemble des voisins du noeud n2)
   'adj_function' => (gettter, is_neighbor_of)
   @ensures: Renvoie un couple (distance_parcouru, chemin_de_toutes_les_villes).
   Crée un le chemin avec une insertion de villes la plus courte possible par rapport à la matrice d'adjacence 
 *)
let rec create_path_nearest_phase2 adj_function adj number_of_city triplet cumulative_distance successor_map current_distance current_city first_city = 
    create_path_operand adj_function adj number_of_city triplet cumulative_distance successor_map current_distance current_city first_city (SetCityDistance.min_elt)


(* @requires: 'successor_map' est un ensemble d'ensembles de la forme : 
   noeud n1 -> (ensemble des voisins du noeud n1)
   noeud n2 -> (ensemble des voisins du noeud n2)
   'adj_function' => (gettter, is_neighbor_of)
   @ensures: Renvoie un couple (distance_parcouru, chemin_de_toutes_les_villes).
   Crée un le chemin avec une insertion de villes la plus longue possible par rapport à la matrice d'adjacence 
 *)
let rec create_path_farthest_phase2 adj_function adj number_of_city triplet cumulative_distance successor_map current_distance current_city first_city = 
    create_path_operand adj_function adj number_of_city triplet cumulative_distance successor_map current_distance current_city first_city (SetCityDistance.max_elt)


(*****************************************************)
(*******************SELECTION DU MODE*****************)
(*****************************************************)
exception Not_Found of string

(* @requires: nécessite la liste des successeurs de chaque ville sous la 
   forme de 'MapCityKeyInt'
   @ensures: si elle est vide, cela veut dire que le fichier n'a pas spécifié
   de successeurs particuliers et que le graphe est complet
*)
let is_phase2 successors = MapCityKeyInt.is_empty successors = false


(* @requires : 'successor_map' est un ensemble d'ensembles de la forme : 
   noeud n1 -> (ensemble des voisins du noeud n1)
   noeud n2 -> (ensemble des voisins du noeud n2)
   'adj_function' => (gettter, is_neighbor_of)
   'name_mode' = RANDOM | NEAREST | FARTHEST 
   @ensures: Renvoie un couple (distance_parcouru, chemin_de_toutes_les_villes) en fonction
   du mode 'name_mode' choisi
   @raises: 'Not_Found' si le 'name_mode' ne correspond à un des valeurs suivantes : 
   RANDOM | NEAREST | FARTHEST 
 *)
let incorp_mode name_mode adj_function adj number_of_city triplet cumulative_distance successor_map current_distance current_city first_city = 
    if is_phase2 successor_map = false then
        if name_mode = "RANDOM" then
            create_path_random adj_function adj triplet current_distance current_city first_city
        else if name_mode = "NEAREST" then
            create_path_nearest adj_function adj triplet current_distance current_city first_city
        else if name_mode = "FARTHEST" then
            create_path_farthest adj_function adj triplet current_distance current_city first_city
        else
            raise (Not_Found name_mode)
    else
        if name_mode = "RANDOM" then
            create_path_random_phase2 adj_function adj number_of_city triplet cumulative_distance successor_map current_distance current_city first_city
        else if name_mode = "NEAREST" then
            create_path_nearest_phase2 adj_function adj number_of_city triplet cumulative_distance successor_map current_distance current_city first_city
        else if name_mode = "FARTHEST" then
            create_path_farthest_phase2 adj_function adj number_of_city triplet cumulative_distance successor_map current_distance current_city first_city
        else
            raise (Not_Found name_mode)
