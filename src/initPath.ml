(**********************************************************************************************************)
module SetAngle = Set.Make(
        struct 
            type t = (float * float) * (float * float) * int
            let compare a b = 
                let (pivot, a1, indexa) = a in
                let (_, a2, indexb) = b in

                let (xp, yp) = pivot in
                let (xa1, ya1) = a1 in
                let (xa2, ya2) = a2 in

                let result = compare ((yp -. ya2 /. (xp -. xa2))) ((yp -. ya2 /. (xp -. xa1))) in

                if  indexa = indexb then
                    0
                else if result = 1 then
                    (-1)
                else if result = (-1) then
                    1
                else
                    0

        end
)
(**********************************************************************************************************)


(* @requires: Nombre de villes non nuls
   @ensures: (Voir le ensures de 'init_path_mode')
*)
let init_one n = 
    let q = Queue.create () in
    let _ = Queue.push 0 q in 

    let state = Array.make n 0 in
    let _ = Array.set state 0 1 in
    (state, q, 1, 0, 0., 0)


(* @requires: Reçoit en argument un 'Array' de villes 
   @ensures: Renvoie la ville dont l'axe y est la plus petite. (La ville la plus à gauche)
 *)
let get_pivot city_coordinate = 
    let rec get_pivot_rec city_coordinate current_index inc_index = 
        let n = Array.length city_coordinate in
        if n = inc_index then
            current_index
        else
            let (_, y, _) = city_coordinate.(inc_index) in
            let (_, y_current, _) = city_coordinate.(current_index) in
            if y < y_current then
                get_pivot_rec city_coordinate inc_index (inc_index + 1)
            else
                get_pivot_rec city_coordinate current_index (inc_index + 1)
    in 
    let final_index = get_pivot_rec city_coordinate 0 0 in
    let (_, y, z) = city_coordinate.(final_index) in
    (y, z, final_index)


(* @requires: Pas de conditions particulières
   @ensures: Renvoie le produit vectoriel entre 3 points
 *)
let produit_vectoriel a b c =
    let (ax, ay) = a in
    let (bx, by) = b in
    let (cx, cy) = c in
    (bx -. ax) *. (cy -. ay) -. (cx -. ax) *. (by -. ay)


(* @requires: Reçoit en argument un 'Array' de villes et un pivot envoyée par la fonction 'get_pivot'
   @ensures: Renvoie un ensemble de villes triées
   Trie les villes en fonction de l'angle de deux villes par rapport aux pivot. La ville avec la plus
   petite angle se placera avant l'autre ville.
 *)
let fill_point_sort city_coordinate pivot = 
    let set_points = SetAngle.empty in
    let (yp, zp, pivot_index) = pivot in
    let (set_angle_finish, _) = Array.fold_left (function elt -> function elt_city_coordinate ->
        let (_, y, z) = elt_city_coordinate in
        let (set_angle, index_city) = elt in

        if index_city = pivot_index then
            (set_angle, index_city + 1)
        else
            (SetAngle.add ((yp, zp) , (y, z), index_city) set_angle, index_city + 1)
    ) (set_points, 0) city_coordinate in
    set_angle_finish



(* @requires: Pas de conditions particulières
   @ensures: Renvoie une mise à jour du chemin parcouru dans le cadre de l'algorithme de 'la
   marche de Graham' sous forme de couple (number_one_state, current_distance) et en modifiant
   les deux piles 's' et 's_coordinate' reçue en argument.
 *)
let rec depile_stack state number_one_state point_elt s s_coordinate getter adj current_distance = 
        if (Stack.length s >= 2) && (Stack.length s_coordinate >= 2) then
            let pile_sommet = Stack.pop s in
            let pile_second = Stack.pop s in

            let _ = Stack.push pile_second s in
            let _ = Stack.push pile_sommet s in

            let pile_sommet_coord = Stack.pop s_coordinate in
            let pile_second_coord = Stack.pop s_coordinate in

            let _ = Stack.push pile_second_coord s_coordinate in
            let _ = Stack.push pile_sommet_coord s_coordinate in
            
            if (produit_vectoriel point_elt pile_second_coord pile_sommet_coord) < 0. then
                    let index = Stack.pop s in
                    let _ = Stack.pop s_coordinate in
                    
                    let _ = Array.set state index 0 in
                    let number_one_state = number_one_state - 1 in

                    (* let _ = Printf.printf "----%f \n" (getter adj pile_sommet pile_second) in *)
                    let current_distance = current_distance -. (getter adj pile_sommet pile_second) in

                    depile_stack state number_one_state point_elt s s_coordinate getter adj current_distance 
            else
                (number_one_state, current_distance)
        else
            (number_one_state, current_distance)


(* @requires: 'city_coordinate' sont les coordonnées en 3d des positions de chaque ville par indexation
   ATTENTION: ne fonctionne que si tous les points sont accessibles (graphe complet de la phase 1)
   @ensures: Renvoie une enveloppe convexe des villes (Voir aussi le ensures de 'init_path_mode')
   Cette algorithme est une implémentation de l'algortihme de 'la marche de Graham'
 *)
let init_convex getter adj city_coordinate =
    let n = Array.length city_coordinate in
    let state = Array.make n 0 in
    let number_one_state = 0 in

    (* On génère le pivot  *)
    let (yp, zp, pivot_index) = get_pivot city_coordinate in
    let set_angle = fill_point_sort city_coordinate (yp, zp, pivot_index) in
    
    (* On crée des piles : une pile d'index et une pile de coordonnées *)
    let s = Stack.create () in
    let s_coordinate = Stack.create () in
    

    let elt = SetAngle.min_elt set_angle in
    let set_angle = SetAngle.remove elt set_angle in

    (* On rajoute dans la pile le pivot et le premier element du set_angle *)
    let _ = Stack.push pivot_index s in
    let _ = Stack.push (yp, zp) s_coordinate in

    let (_, point, index) = elt in

    let _ = Stack.push index s in
    let _ = Stack.push point s_coordinate in


    let _ = Array.set state index 1 in
    let _ = Array.set state pivot_index 1 in
    let number_one_state = number_one_state + 2 in

    let current_distance = getter adj pivot_index index in
    (* let _ = Printf.printf "++++%f \n" (getter adj pivot_index index) in *)
    let list_set_angle = SetAngle.elements set_angle in
    let rec init_convex_rec list_set_angle s s_coordinate number_one_state current_distance = 
        (* let _ = Stack.iter (function x ->
        Printf.printf "%s " city_array.(x).(0)
    ) s in let _ = Printf.printf "\n" in *)
    match list_set_angle with
         
        |[] -> (s,number_one_state, current_distance)
        |x::t ->
            let (_, point_elt, index_elt) = x in

            (* let _ = SetAngle.iter (function a -> 
            let (_, _, index) = a in
                Printf.printf "Ville : %s\n" city_array.(index).(0)
            ) set_angle in
            let _ = Printf.printf "%s\n" city_array.(index_elt).(0) in *)

            let (number_one_state, current_distance) = depile_stack state number_one_state point_elt s s_coordinate getter adj current_distance in
            
            let old_index = Stack.pop s in
            let _ = Stack.push old_index s in
            
            let _ = Stack.push index_elt s in
            let _ = Stack.push point_elt s_coordinate in
            
            let _ = Array.set state index_elt 1 in
            let number_one_state = number_one_state + 1 in

            
            (* let _ = Printf.printf "++++%f \n" (getter adj index_elt old_index) in *)
            let current_distance = current_distance +. (getter adj index_elt old_index) in
            
            init_convex_rec t s s_coordinate number_one_state current_distance

    in 
    let (s,number_one_state, final_distance) = init_convex_rec list_set_angle s s_coordinate number_one_state current_distance in
    let q = Queue.create () in
    (* let _ = Printf.printf " --- %d ---" number_one_state in *)
    let first_city = Stack.pop s in
    let _ = Queue.push first_city q in

    let _ = Stack.iter (function x ->
        Queue.push x q
    ) s in 


     (* let _ = Queue.iter (function x ->
        Printf.printf "%s " city_array.(x).(0)
    ) q in let _ = Printf.printf "\n" in *)


    (* pivot_index devient la derniere ville visite dans la file *)
    (state, q, number_one_state, first_city, final_distance, pivot_index)


(*****************************************************)
(*******************SELECTION DU MODE*****************)
(*****************************************************)
exception Not_Found of string


(* @requires : 'getter' -> permet de récupérer une valeur de distance de la matrice d'adjacencen 'adj'
   'name_mode' = ONE | HULL 
   @ensures: Renvoie (state, q, number_one_state, first_city, final_distance, pivot_index) en fonction
   du mode 'name_mode' choisi
   'state' correpond à un Array donc chaque case (l'indexation) correspond à une ville. Le nombre 
   indiqué correspond au nombre de fois que la ville a été visitée.
   'q' est le début de la tournée sous la forme d'une file.
   'number_one_state' le nombre de ville qui a été visitée une seule fois.
    'first_city' la première ville de la tournée.
    'final_distance' la distance parcourue du chemin initial.
    'pivot_index' qui correpond plus simplement à la dernière ville de la tournée
   @raises: 'Not_Found' si le 'name_mode' ne correspond à un des valeurs suivantes : 
   ONE | HULL 
 *)
let init_path_mode name_mode getter adj city_coordinate is_phase2 = 
    if is_phase2 then
        init_one (Array.length city_coordinate)
    else
        if name_mode = "ONE" then
            init_one (Array.length city_coordinate)
        else if name_mode = "HULL" then
            init_convex getter adj city_coordinate
        else
            raise (Not_Found name_mode)