(* Value of pi *)
let pi = acos (-1.);;


(* value of radius on earth *)
let radius_earth = 6371009.;;


(* @requires: le rayon 'r' doit être positif
   @ensures: retourne des coordonnées cartésiennes 'float * float * float' 
   Calcul les coordonnées cartésiennes à partir des
   coordonnées spériques longitude / latitude
*)
let point_of_speric_coordinate a r = 
    let (longitude, latitude) = a in
        let longitude = (longitude *. pi) /. 180. in
        let latitude = (latitude *. pi) /. 180. in
        (r *. (cos longitude) *. (cos latitude), r *. (cos latitude) *. (sin longitude), r *. (sin latitude));;


(* @requires: requiert un array de la forme envoyée en sortie par la 
   fonction 'extract_city_array' du module 'ManagerFileCity'. Elle foit être 
   de cetet forme [|
   (_, float, float);
   (_, float, float);
   ...
   |]
   @ensures: renvoie un float array qui renvoie la liste des coordonnées cartésiennes
   des villes par ordre d'index. 
  *)
let extract_coordinate city_array = 
        let give_coordinate index value = 
            let long = float_of_string value.(1) in
            let lat = float_of_string value.(2) in
            
            (point_of_speric_coordinate (long, lat) radius_earth) in
           
        Array.mapi give_coordinate city_array;;


(* @requires: deux point avec coordonnées cartésiennes
   @ensures: retourne une distance entre les deux points
   Calcul de la distance entre deux point de coordonnées cartésiennes.
*)
let euclidean_distance3d a b  = 
    let (x_a, y_a, z_a) = a in
    let (x_b, y_b, z_b) = b in 
        let x = (x_b -. x_a) ** 2. in
        let y = (y_b -. y_a) ** 2. in
        let z = (z_b -. z_a) ** 2. in
            sqrt (x +. y +. z);;