open AdjacencyMatrixType

module Make (Graph : GraphType) = 
    struct
        type elt = Graph.elt
        type adj_matrix = elt array array


        let create_matrix_adj n e = 
            Array.make_matrix n n e

        let length adj = 
            Array.length adj

        let get adj i j = 
            adj.(i).(j) 


        let set adj i j e = 
            Array.set (adj.(i)) j e


         let iteri f adj = 
            let rec iteri_rec adj i j = 
                let n = length adj in
                if i < n then
                    if j < n then
                        let value = get adj i j in
                        let _ = f i j value in
                        iteri_rec adj i (j + 1)
                    else
                        iteri_rec adj (i + 1) 0
                else
                    ()
            in iteri_rec adj 0 0

        let is_neighbor_of adj i j =
            Graph.is_neighbor_of (get adj i j)
        
        (* val make_undirected_matrix : (int -> int -> 'a array -> elt) -> 'a array -> elt -> adj_matrix *)
        let make_undirected_matrix f node_array e = 
            let n = Array.length node_array in
            let m = create_matrix_adj n e in
                let rec fill_array f m node_array index current_index = 
                    let n = Array.length node_array in
                    if index < n then 
                        if current_index < n then
                            let value = f index current_index node_array in
                            set m index current_index value ;  
                            set m current_index index value ;
                            fill_array f m node_array index (current_index + 1)
                        else 
                            fill_array f m node_array (index + 1) (index + 1)
                    else
                        () 
                in
            fill_array f m node_array 0 0 ;
            m
    end 