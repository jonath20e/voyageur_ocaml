(* Une matrice d'adjacence est une matrice qui stocke des informations sur 
   un graphe fini et orienté.
   Chaque case de la matrice renseigne sur le poids ou le coût entre deux noeuds
   'i' et 'j'.
   Exemple: on prends l'exemple d'un suite de villes tous connectées entre eux
   et indicés par des nombre, avec comme coût la distance qui les sépapre. 
   La matrice d'adjacence vaut alors : 
   [|
     [|-1; 1; 3|];
     [|10; -1; 3|];
     [|1; 1; -1|];
   |]
   Le '-1' ici représente un accès impossible que l'on peut implémenter dans is_neighbor_of
   de GraphType.
   Note : on doit savoir à l'avance la taille du graphe car on ne peut rajouter des sommets
   ici, simplement changer les arcs.
 *)
open AdjacencyMatrixType
 
(* On définit un foncteur d'une matrice d'adjacence *)
module Make (Graph : GraphType) : A with type elt = Graph.elt
 