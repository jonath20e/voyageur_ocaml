(* On définit les types nécessaires à une matrice d'adjacence *)

module type GraphType =
  sig
    type elt
    (* Type des éléments de l a matrice d'adjcaence *)


    val is_neighbor_of : elt -> bool
    (* On dit que les noeuds 'i' et 'j' sont voisins entre eux si la valeur 
       retournée par la  position 'i' et 'j' vaut true dans 'is_neighbor_of' 
     *) 
  end

module type A = 
    sig  
        type elt
        (* Le type de l'élément de la matrice d'adjacence *)
         

        type adj_matrix
        (* La matrice d'adjacence est une matrice en deux dimensions composées uniquement du type elt *)
        

        val create_matrix_adj : int -> elt -> adj_matrix
        (* @requires: un entier positif
           @ensures: Renvoie une variavle de type 'adj_matrix' qui est remplie
           de l'élément 'elt' envoyé en paramètres
           @raises: exceptions possibles issues du module 'Array'
           Créee une matrice d'adjacence à partir d'un objet qui peut correspondre 
           à nimporte quel type (Exemple : une liste, un fichier, etc ).
         *)
        
         val length : adj_matrix -> int


        val get : adj_matrix -> int -> int -> elt
        (* @requires: la matrice 'adj_matrix' et 
           deux entiers positifs et inférieurs à la taille de 'adj_matrix'
           @ensures: Renvoie un type 'elt' associée aux valuers de la conditions
           @raises: exceptions possibles issues du module 'Array'
           Renvoie la valeur de la matrix d'adjacence à la position int * int *)


        val set : adj_matrix -> int -> int -> elt -> unit
        (* @requires: pas de conditions particulières
           deux entiers positifs et inférieurs à la taille de 'adj_matrix'
           @raises: exceptions possibles issues du module 'Array'
           Change la matrice avec la nouvelle valeur à la position int -> int donnée *)

        val iteri : (int -> int -> elt -> unit) -> adj_matrix -> unit


        val is_neighbor_of : adj_matrix -> int -> int -> bool
        (* @requires: deux entiers positifs et inférieurs à la taille de 'adj_matrix'
           @ensures: Renvoie un booléen sur l'état de la matrice aux valeurs indiquées
           @raises: exceptions possibles issues du module 'Array'
           Renvoie vrai si le premier élément est voisin du second élément *)


        val make_undirected_matrix : (int -> int -> 'a array -> elt) -> 'a array -> elt -> adj_matrix
    end
