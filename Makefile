SRC_FILES_CMI_CMO = src

EXEC = circuit.exe
EXEC_TEST = test.exe

OCAMLC = ocamlc
OCAMLC_CMD = $(OCAMLC) -I $(SRC_FILES_CMI_CMO) -c 
# SOURCES = main.ml adjacencyMatrix.mli adjacencyMatrix.ml adjacencyMatrixType.mli 
SOURCE_CMO_FOR_TEST = geometry.cmo adjacencyMatrix.cmo initPath.cmo incorporation.cmo optimization.cmo managerFileCity.cmo
SOURCES_CMO_SRC_FOR_TEST = $(SRC_FILES_CMI_CMO)/geometry.cmo $(SRC_FILES_CMI_CMO)/adjacencyMatrix.cmo $(SRC_FILES_CMI_CMO)/initPath.cmo $(SRC_FILES_CMI_CMO)/incorporation.cmo $(SRC_FILES_CMI_CMO)/optimization.cmo $(SRC_FILES_CMI_CMO)/managerFileCity.cmo

SOURCES_CMO = geometry.cmo adjacencyMatrix.cmo initPath.cmo incorporation.cmo optimization.cmo managerFileCity.cmo main.cmo 
SOURCES_CMO_SRC = $(SRC_FILES_CMI_CMO)/geometry.cmo $(SRC_FILES_CMI_CMO)/adjacencyMatrix.cmo $(SRC_FILES_CMI_CMO)/initPath.cmo $(SRC_FILES_CMI_CMO)/incorporation.cmo $(SRC_FILES_CMI_CMO)/optimization.cmo $(SRC_FILES_CMI_CMO)/managerFileCity.cmo $(SRC_FILES_CMI_CMO)/main.cmo 

all: circuit

.PHONY: run-test

run-test: test.exe
	./$(EXEC_TEST)

test: $(SOURCE_CMO_FOR_TEST) test.cmo
	$(OCAMLC) -I test $(SOURCES_CMO_SRC_FOR_TEST) test/test.cmo  -o $(EXEC_TEST)

test.cmo: adjacencyMatrix.cmo initPath.cmo incorporation.cmo optimization.cmo $(SRC_FILES_CMI_CMO)/main.ml
	$(OCAMLC) -I test -I src -c test/test.ml

circuit: $(SOURCES_CMO)
	$(OCAMLC) $(SOURCES_CMO_SRC) -o $(EXEC)

main.cmo: adjacencyMatrix.cmo initPath.cmo incorporation.cmo optimization.cmo $(SRC_FILES_CMI_CMO)/main.ml
	$(OCAMLC_CMD) $(SRC_FILES_CMI_CMO)/main.ml

managerFileCity.cmo: adjacencyMatrix.cmo initPath.cmo incorporation.cmo optimization.cmo $(SRC_FILES_CMI_CMO)/managerFileCity.ml
	$(OCAMLC_CMD) $(SRC_FILES_CMI_CMO)/managerFileCity.ml

initPath.cmo: $(SRC_FILES_CMI_CMO)/initPath.ml
	$(OCAMLC_CMD) $(SRC_FILES_CMI_CMO)/initPath.ml

incorporation.cmo: $(SRC_FILES_CMI_CMO)/incorporation.ml
	$(OCAMLC_CMD) $(SRC_FILES_CMI_CMO)/incorporation.ml

optimization.cmo: $(SRC_FILES_CMI_CMO)/optimization.ml
	$(OCAMLC_CMD) $(SRC_FILES_CMI_CMO)/optimization.ml

geometry.cmo: $(SRC_FILES_CMI_CMO)/geometry.ml
	$(OCAMLC_CMD) $(SRC_FILES_CMI_CMO)/geometry.ml

adjacencyMatrix.cmo: adjacencyMatrixType.cmi adjacencyMatrix.cmi $(SRC_FILES_CMI_CMO)/adjacencyMatrix.ml
	$(OCAMLC_CMD) $(SRC_FILES_CMI_CMO)/adjacencyMatrix.ml

adjacencyMatrix.cmi: adjacencyMatrixType.cmi $(SRC_FILES_CMI_CMO)/adjacencyMatrix.mli
	$(OCAMLC) -I $(SRC_FILES_CMI_CMO) $(SRC_FILES_CMI_CMO)/adjacencyMatrix.mli

adjacencyMatrixType.cmi: $(SRC_FILES_CMI_CMO)/adjacencyMatrixType.mli
	$(OCAMLC) -I $(SRC_FILES_CMI_CMO) $(SRC_FILES_CMI_CMO)/adjacencyMatrixType.mli

clean:
	rm $(SRC_FILES_CMI_CMO)/*.cm[io]
	rm $(EXEC)
	rm $(EXEC_TEST)