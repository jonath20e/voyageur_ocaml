# Projet d'ocaml 2018 : Planificateur de tournée efficace

## Installation
Pour compiler le projet, il suffit de se placer à la racine du projet et d'exécuter la commande suivante :   
```
make circuit
```

## Utilisation
Après compilation, le projet peut se lancer via : 
```
./circuit.exe "fichier_de_parametrage" "fichier_contenant_les_villes"
```
Cette commande fonctionne aussi bien pour la phase 1 que la phase 2.  
 
 \
Le *fichier_de_parametrage* doit être de la forme :  
```
ONE | HULL
RANDOM | NEAREST | FARTHEST
REPOSITIONNEMENT | INVERSION
```
Le *fichier_contenant_les_villes* peut être de la forme (phase 1): 
```
n
<nom de la ville> <longitude> <lattitude> [n fois]
```
ou bien (phase 2): 
```
n
<nom de la ville> <longitude> <lattitude> [n fois]
départ> : <arrivée_1> ... <arrivée_k> [n fois]
```
**ATTENTION** : l'option *HULL* marche **uniquement** qu'avec la phase 1
## Test
Il faut d'abord compiler l'exécutable de tests par la commande suivante : 
```
make test
```
Pour exécuter les tests, il suffit de se placer à la racine du projet et d'exécuter la commande suivante : 
```
make run-test
```
## Exigences
Pour compiler, le projet utiliser les modules `Array`, `Set`, `Map` et `Printf`.
## Auteur
Boaknin Jonathan : <jonathan.boaknin@ensiie.fr>