open ManagerFileCity
open Printf


(* @requires: 'tests' est un 'Array' de tests dont chaque case correspond à :  
   - on retrouve le nom du test
   - la sortie d'une fonction à tester qui renvoie une valeur
   - le test lui-même qui reçoit la valeur de la fonction précédente et renvoie
     true si le test a réussi, false sinon
   @ensures: 'Assert_failure' qui est traitée par la suite
 *)
let run_test name_set_test tests = 
    let n = Array.length tests in
    let _ = printf "\nLancement du jeu de test(s) sur %s (%d test(s))\n" name_set_test n in
    Array.iteri (function index -> function test ->
        let (name_test, result_test_fun, check_test) = test in
        let _ = printf "%s" name_test in
        let result_test = result_test_fun () in
        let cond = check_test result_test in
            try
                let _ = assert cond in
                printf ":OK\t(%d/%d)\n" (index + 1) n
                    
            with Assert_failure  _ -> printf ":Test has failed \t(%d/%d)\n" (index + 1) n
    ) tests


(* LANCEMENT DES TESTS *)
let _ = 
    (**************************************************************)
    (* Tests sur le foncteur 'TurnCity' du module ManagerFileCity *)
    (**************************************************************)
    let test_turn_city_length = [|
        ("test_length1", 
            (function () -> 
                let init_mat_test = TurnCity.create_matrix_adj 5 0. in
                TurnCity.length init_mat_test
            ), 
            (function result -> result = 5)
        );
        ("test_length2", 
            (function () -> 
                let init_mat_test = TurnCity.create_matrix_adj 15 0. in
                TurnCity.length init_mat_test
            ), 
            (function result -> result = 15)
        )
    |] in 
    let _ = run_test "Foncteur 'TurnCity' du module ManagerFileCity (length)" test_turn_city_length in

    let test_turn_city_get_and_set = [|
        ("test_get1", 
            (function () -> 
                let init_mat_test = TurnCity.create_matrix_adj 2 5. in
                TurnCity.get init_mat_test 1 0
            ), 
            (function result -> result = 5.)
        );
        ("test_get2", 
            (function () -> 
                let init_mat_test = TurnCity.create_matrix_adj 2 0. in
                let _ = TurnCity.set init_mat_test 1 0 15. in
                TurnCity.get init_mat_test 1 0
            ), 
            (function result -> result = 15.)
        );
        ("test_get3", 
            (function () -> 
                let init_mat_test = TurnCity.create_matrix_adj 2 0. in
                let _ = TurnCity.set init_mat_test 1 0 15. in
                TurnCity.get init_mat_test 0 0
            ), 
            (function result -> result = 0.)
        )
    |] in
    let _ = run_test "Foncteur 'TurnCity' du module ManagerFileCity (get and set)" test_turn_city_get_and_set in

    let test_turn_city_is_neighbor_of = [|
        ("test_voisin1", 
            (function () -> 
                let init_mat_test = TurnCity.create_matrix_adj 2 5. in
                TurnCity.is_neighbor_of init_mat_test 1 0
            ), 
            (function result -> result = true)
        );
        ("test_voisin2", 
            (function () -> 
                let init_mat_test = TurnCity.create_matrix_adj 2 (-1.) in
                TurnCity.is_neighbor_of init_mat_test 1 0
            ), 
            (function result -> result = false)
        )
    |] in
    let _ = run_test "Foncteur 'TurnCity' du module ManagerFileCity (is_neighbor_of)" test_turn_city_is_neighbor_of in

    let test_turn_city_undirected_matrix = [|
        ("test_make_undirected_matrix1", 
            (function () -> 
                    let adj = TurnCity.make_undirected_matrix (function x -> function y -> function node_array ->
                        1.
                    ) [|(1.);(2.);(3.)|] (-1.) in  
                    adj
            ), 
            (function result -> 
                TurnCity.get result 0 1 = TurnCity.get result 1 0 &&
                TurnCity.get result 0 2 = TurnCity.get result 2 0 &&
                TurnCity.get result 2 1 = TurnCity.get result 1 2
            )
        );
        ("test_make_undirected_matrix2", 
            (function () -> 
                    let adj = TurnCity.make_undirected_matrix (function x -> function y -> function node_array ->
                        node_array.(x) +. node_array.(y)
                    ) [|(1.);(2.);(3.)|] (-1.) in  
                    adj
            ), 
            (function result -> 
                TurnCity.get result 0 1 = TurnCity.get result 1 0 && (TurnCity.get result 1 0) = 3. && 
                TurnCity.get result 0 2 = TurnCity.get result 2 0 && (TurnCity.get result 2 0) = 4. && 
                TurnCity.get result 2 1 = TurnCity.get result 1 2 && (TurnCity.get result 1 2) = 5.
            )
        )
    |] in
    let _ = run_test "Foncteur 'TurnCity' du module ManagerFileCity (undirect matrix)" test_turn_city_undirected_matrix in
    (*****************************************************)
    (* Tests sur les fonctions du module ManagerFileCity *)
    (*****************************************************)
        let _ = printf "\n*****************************************************************************\n" in
        let test_manager_city_explode = [|
        ("test_explode1", 
            (function () -> 
                explode ' ' ""
            ), 
            (function result -> result = [] 
            )
        );
        ("test_explode2", 
            (function () -> 
                explode ' ' "Paris"
            ), 
            (function result -> result = ["Paris"] 
            )
        );
        ("test_explode3", 
            (function () -> 
                explode ' ' "Paris Lyon Marseille"
            ), 
            (function result -> result = ["Paris";"Lyon";"Marseille"] 
            )
        );
        ("test_explode4", 
            (function () -> 
                explode ' ' "Paris Lyon Marseille "
            ), 
            (function result -> result = ["Paris";"Lyon";"Marseille"] 
            )
        );
        ("test_explode5", 
            (function () -> 
                explode '-' "Paris-Lyon-Marseille"
            ), 
            (function result -> result = ["Paris";"Lyon";"Marseille"] 
            )
        )
        |] in
        let _ = run_test "Foncctions du module ManagerFileCity (explode)" test_manager_city_explode in

        let test_manager_city_explode_for_successor = [|
        ("test_explode_for_successor1", 
            (function () -> 
                let city_index = MapCity.empty in
                let city_index = MapCity.add "Paris" 0 city_index in
                let city_index = MapCity.add "Lyon" 1 city_index in
                let city_index = MapCity.add "Marseille" 2 city_index in
                explode_for_successor  "Paris : Lyon Marseille" Incorporation.MapCityKeyInt.empty city_index
            ), 
            (function result -> let succ = Incorporation.MapCityKeyInt.find 0 result in
            Incorporation.SetCity.elements succ = [1;2]
            )
        );
        ("test_explode_for_successor2", 
            (function () -> 
                let city_index = MapCity.empty in
                let city_index = MapCity.add "Paris" 0 city_index in
                let city_index = MapCity.add "Lyon" 1 city_index in
                let city_index = MapCity.add "Marseille" 2 city_index in
                explode_for_successor  "Paris : Lyon" Incorporation.MapCityKeyInt.empty city_index
            ), 
            (function result -> let succ = Incorporation.MapCityKeyInt.find 0 result in
            Incorporation.SetCity.elements succ = [1]
            )
        )  
        |] in
        let _ = run_test "Foncctions du module ManagerFileCity (explode_for_successor)" test_manager_city_explode_for_successor in

        let test_manager_city_extract_city_array = [|
        ("test_phase1", 
            (function () -> 
                extract_city_array "test/city_cut"
            ), 
            (function result -> let (city_array, succ_for_phase2) = result in
                Incorporation.is_phase2 succ_for_phase2 = true
            )
        );
        ("test_phase2", 
            (function () -> 
                extract_city_array "test/city_complete"
            ), 
            (function result -> let (city_array, succ_for_phase2) = result in
                Incorporation.is_phase2 succ_for_phase2 = false
            )
        );
        ("test_extract_city_array1", 
            (function () -> 
                extract_city_array "test/city_complete"
            ), 
            (function result -> let (city_array, succ_for_phase2) = result in
                city_array.(0).(0) = "Paris" &&
                city_array.(1).(0) = "Lyon" &&
                city_array.(2).(0) = "Bordeaux" &&
                city_array.(3).(0) = "Amiens" &&
                city_array.(4).(0) = "Marseille"
            )
        );
        ("test_extract_city_array2", 
            (function () -> 
                extract_city_array "test/city_cut"
            ), 
            (function result -> let (city_array, succ_for_phase2) = result in
                city_array.(0).(0) = "Paris" &&
                city_array.(1).(0) = "Lyon" &&
                city_array.(2).(0) = "Bordeaux" &&
                city_array.(3).(0) = "Amiens" &&
                city_array.(4).(0) = "Marseille"
            )
        )
        |] in
        let _ = run_test "Foncctions du module ManagerFileCity (extract_city_array)" test_manager_city_extract_city_array in    

        (**********************************************)
        (* Tests sur les fonctions du module Geometry *)
        (**********************************************)
        let _ = printf "\n*****************************************************************************\n" in
        let test_geometry_euclidean_distance3d = [|
        ("test_euclidean_distance3d1", 
            (function () -> 
                Geometry.euclidean_distance3d (1., 1., 1.) (1., 1., 1.)
            ), 
            (function result -> result = 0.
            )
        );   
        ("test_euclidean_distance3d2", 
            (function () -> 
                Geometry.euclidean_distance3d (1., 1., 1.) (2., 1., 1.)
            ), 
            (function result -> result = 1.
            )
        )   
        |] in
        let _ = run_test "Foncctions du module Geometry (euclidean3d)" test_geometry_euclidean_distance3d in    
        (* Les autres fonctions sont assez explicites dans la formulation pour se passer de tests *)


        (**********************************************)
        (* Tests sur les fonctions du module InitPath *)
        (**********************************************)
        
    ()

    (* let path_test = Queue.copy path_out in
    let _ = Queue.pop path_test in 
    ²let (_, distance) = Queue.fold (function accu -> function x -> 
        let (node, distance) = accu in
        let distance = distance +. (TurnCity.get adj node x) in 
        (x, distance)
    ) (0, (0.)) 
    path_test in
    printf "%f \n" distance ; *)